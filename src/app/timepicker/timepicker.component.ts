import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { Moment } from "moment";
import * as moment from "moment";
import { DataService } from '../shared/data.service';

@Component({
  selector: "app-timepicker",
  templateUrl: "./timepicker.component.html",
  styleUrls: ["./timepicker.component.sass"]
})
export class TimepickerComponent {
  @Input() dataSource;
  @Input() checker: boolean;
  @ViewChild('date', {static: false}) date: ElementRef

  constructor(private dataService: DataService) {}

  dateFilter(date: Moment) {
    if (
      date.isSameOrAfter(moment().subtract(1,"days")) &&
      date.isSameOrBefore(moment().add(6, "days"))
    ) {
      return true;
    }
  }

  onChange($event) {
    this.dataService.selectDate(true)
    return console.log($event);
  }
}
