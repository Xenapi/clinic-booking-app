import { Component, Input, OnDestroy } from '@angular/core';
import { Clinic } from '../shared/clinic.model';
import { DataService } from '../shared/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-clinic-service',
  templateUrl: './clinic-service.component.html',
  styleUrls: ['./clinic-service.component.sass']
})
export class ClinicServiceComponent implements OnDestroy {

  @Input() dataSource: Promise<Clinic[]>
  selectedClinic: number

  subscription: Subscription

  constructor(dataService: DataService) {
    dataService.selectedClinic.subscribe( clinic => this.selectedClinic = clinic )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }
}
