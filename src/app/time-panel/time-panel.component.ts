import { Component, OnDestroy } from '@angular/core';
import { Time } from '../shared/time.model';
import { TimesService } from '../times.service';
import { Clinic } from '../shared/clinic.model';
import { DataService } from '../shared/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-time-panel",
  templateUrl: "./time-panel.component.html",
  styleUrls: ["./time-panel.component.sass"]
})
export class TimePanelComponent implements OnDestroy{
  timeData: Array<Time>;
  clinics: Array<Clinic>

  subscriptions: Subscription

  constructor( timeService: TimesService, dataService: DataService) {
    timeService.getTimes().then(data => (this.timeData = data));
    dataService.getClinics().subscribe({
      next: data => (this.clinics = data),
      error: error => (console.log(error)),
      complete: () => console.log("completed")

    });
  }

  setColor(value) {
    if (value === "available") {
      return "green";
    } else if (value === "reserved") {
      return "purple";
    } else {
      return "red";
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe()
  }
}
