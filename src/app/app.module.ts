import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormComponent } from './form/form.component';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { ClinicServiceComponent } from './clinic-service/clinic-service.component';
import { TimePanelComponent } from './time-panel/time-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    TimepickerComponent,
    ClinicServiceComponent,
    TimePanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
