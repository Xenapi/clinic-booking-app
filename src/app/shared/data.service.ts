import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Clinic } from './clinic.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class DataService {

  private clinicSource = new BehaviorSubject<number>(0)
  selectedClinic = this.clinicSource.asObservable()

  private dateSource = new BehaviorSubject<boolean>(false)
  isDateSelected = this.dateSource.asObservable()

  constructor(private http: HttpClient) {}

  getClinics() {
    const path = "https://next.json-generator.com/api/json/get/Ey5vMsevP";
    return this.http.get<Clinic[]>(path);
  }

  changeClinic(clinic: number) {
    return this.clinicSource.next(clinic)
  }

  selectDate(date: boolean) {
    return this.dateSource.next(date)
  }
}
