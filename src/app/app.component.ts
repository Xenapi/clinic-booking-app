import { Component, OnInit, AfterViewChecked, OnDestroy } from "@angular/core";
import { Clinic } from "./shared/clinic.model";
import { FormControl } from "@angular/forms";
import { TimesService } from './times.service';
import { Time } from './shared/time.model';
import { Subscription } from 'rxjs';
import { DataService } from './shared/data.service';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.sass"]
})
export class AppComponent implements OnInit, OnDestroy {
  clinics: Array<Clinic>;
  times: Array<Time>;
  myControl = new FormControl();
  subscription: Subscription;

  isClinicSelected = false;
  isDateSelected: boolean;

  constructor(
    dataService: DataService,
    private timesService: TimesService,
  ) {
    dataService.getClinics().subscribe(data => (this.clinics = data));
    dataService.isDateSelected.subscribe(date => this.isDateSelected = date)
  }

  ngOnInit(): void {
    this.timesService.getTimes().then(data => (this.times = data));
  }
  ngOnDestroy() {
    this.subscription.unsubscribe()
  }
}
