import { Component, Output, OnDestroy } from "@angular/core";
import { Clinic } from "../shared/clinic.model";
import { DataService } from "../shared/data.service";
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"]
})
export class FormComponent implements OnDestroy {
  @Output() isChanged: boolean;
  clinics: Array<Clinic>;

  subscription: Subscription

  constructor(private dataService: DataService) {
    dataService.getClinics().subscribe(clinics => (this.clinics = clinics));
  }

  changeClinic(event) {
    if (event.isUserInput) {
      this.isChanged = true;
      return this.dataService.changeClinic(event.source.value);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }
}
